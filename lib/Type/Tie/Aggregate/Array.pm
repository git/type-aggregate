# ABSTRACT: class to tie arrays for Type::Tie::Aggregate

######################################################################
# Copyright (C) 2021 Asher Gordon <AsDaGo@posteo.net>                #
#                                                                    #
# This program is free software: you can redistribute it and/or      #
# modify it under the terms of the GNU General Public License as     #
# published by the Free Software Foundation, either version 3 of     #
# the License, or (at your option) any later version.                #
#                                                                    #
# This program is distributed in the hope that it will be useful,    #
# but WITHOUT ANY WARRANTY; without even the implied warranty of     #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU   #
# General Public License for more details.                           #
#                                                                    #
# You should have received a copy of the GNU General Public License  #
# along with this program. If not, see                               #
# <http://www.gnu.org/licenses/>.                                    #
######################################################################

package Type::Tie::Aggregate::Array;

=head1 DESCRIPTION

This class is used to tie arrays. This class is internal to
L<Type::Tie::Aggregate|Type::Tie::Aggregate>.

=cut

use v5.6.0;
use strict;
use warnings;
use namespace::autoclean;
use parent 'Type::Tie::Aggregate::Base';

sub _create_ref { shift; \@_ }

sub _check_value {
    my (undef, $value) = @_;
    return 'Not an ARRAY reference' unless ref $value eq 'ARRAY';
    return;
}

sub TIEARRAY { my $class = shift; $class->_new(@_) }

__PACKAGE__->_install_methods(
    { mutates => 1 },
    STORESIZE	=> '$#$ref = $_[0] - 1',
    STORE	=> '$ref->[$_[0]] = $_[1]',
    CLEAR	=> '@$ref = ()',
    POP		=> 'pop @$ref',
    PUSH	=> 'push @$ref, @_',
    SHIFT	=> 'shift @$ref',
    UNSHIFT	=> 'unshift @$ref, @_',
    SPLICE	=> '&CORE::splice($ref, @_)',
    DELETE	=> 'delete $ref->[$_[1]]',
);

__PACKAGE__->_install_methods(
    { mutates => 0 },
    FETCHSIZE	=> '@$ref',
    FETCH	=> '$ref->[$_[0]]',
    EXISTS	=> 'exists $ref->[$_[0]]',
    EXTEND	=> sub {},
);

=head1 SEE ALSO

=for :list
* L<Type::Tie::Aggregate>

=cut

1;

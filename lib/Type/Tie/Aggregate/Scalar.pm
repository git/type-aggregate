# ABSTRACT: class to tie arrays for Type::Tie::Aggregate

######################################################################
# Copyright (C) 2021 Asher Gordon <AsDaGo@posteo.net>                #
#                                                                    #
# This program is free software: you can redistribute it and/or      #
# modify it under the terms of the GNU General Public License as     #
# published by the Free Software Foundation, either version 3 of     #
# the License, or (at your option) any later version.                #
#                                                                    #
# This program is distributed in the hope that it will be useful,    #
# but WITHOUT ANY WARRANTY; without even the implied warranty of     #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU   #
# General Public License for more details.                           #
#                                                                    #
# You should have received a copy of the GNU General Public License  #
# along with this program. If not, see                               #
# <http://www.gnu.org/licenses/>.                                    #
######################################################################

package Type::Tie::Aggregate::Scalar;

=head1 DESCRIPTION

This class is used to tie arrays. This class is internal to
L<Type::Tie::Aggregate|Type::Tie::Aggregate>.

The C<_value> method is overloaded to return and set the referenced
scalar, rather than the reference itself. C<< $obj->_value >> is like
C<< ${$obj->_ref} >>.

=cut

use v5.6.0;
use strict;
use warnings;
use namespace::autoclean;
use Carp;
use parent 'Type::Tie::Aggregate::Base';

sub _create_ref {
    shift;
    carp 'More than one value given for scalar; using first value'
	if @_ > 1;
    \$_[0];
}

sub _value {
    my $self = shift;
    my $ref = $self->_ref;
    return $$ref unless @_;
    ($$ref) = @_;
}

sub TIESCALAR { my $class = shift; $class->_new(@_) }

__PACKAGE__->_install_methods(
    { mutates => 1 },
    STORE	=> '$$ref = $_[0]',
);

__PACKAGE__->_install_methods(
    { mutates => 0 },
    FETCH	=> '$$ref',
);

=head1 SEE ALSO

=for :list
* L<Type::Tie::Aggregate>

=cut

1;
